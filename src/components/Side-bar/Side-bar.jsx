import React from "react";
import LinkNav from "../LinkNav/LinkNav";
// import FriendsPanel from "./Friends-panel";
import cover from "../../style/cover.jpg";

const SideBar = () => {
    return (
        <div className='side-bar'>
            <div className='side-bar__header'>
                <div className='side-bar__header--icon'>
                    <img
                        src={cover}
                        alt=''
                    />
                </div>
                <div className='side-bar__header--user-name'>Roy Monroe</div>
            </div>
            <nav className='side-bar__nav'>
                <LinkNav to='/Profile'>Profile</LinkNav>
                <LinkNav to='/Messages'>Messages</LinkNav>
                <LinkNav to='/News'>News</LinkNav>
                <LinkNav to='/Music'>Music</LinkNav>
                <LinkNav to='/Search'>Search</LinkNav>
            </nav>
            <div className='friend-list'>
                <div className='friend-list__title'>Friends</div>
                {/* <FriendsPanel state={props} /> */}
            </div>
        </div>
    );
};

export default SideBar;
