import React from "react";
import cover from "../../style/cover.jpg";
const FriendsPanel = (props) => {
  // console.log(props);
  let friendsElement = props.state.fD.map((friend, index) => {
    return (
      <div className='friend' key={index} id={friend.id}>
        <img
          className='avatar'
          src={cover}
          alt={friend.name}
        />
        <div className='friend-name'>{friend.name}</div>
      </div>
    );
  });
  return <div className='friend-list__friends'>{friendsElement}</div>;
};

export default FriendsPanel;
