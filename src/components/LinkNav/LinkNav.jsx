import React from "react";
import {NavLink} from "react-router-dom";
import cover from "../../style/cover.jpg";


const LinkNav = ({children, to}) => {
    return (
        <li className="side-bar__nav--links">
            <img
                src={cover}
                alt=""
            />
            <NavLink to={to}>{children}</NavLink>
        </li>
    
    );
};

export default LinkNav;
