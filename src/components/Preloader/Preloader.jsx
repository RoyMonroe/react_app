import React from 'react';

let Preloader = () => (
        <div className="lds-grid">
            <div> </div>
            <div> </div>
            <div> </div>
            <div> </div>
            <div> </div>
            <div> </div>
            <div> </div>
            <div> </div>
            <div> </div>
        </div>

)


export default Preloader;
