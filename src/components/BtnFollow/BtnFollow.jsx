import React from 'react';
import "./_BtnFollow.scss";

const BtnFollow = ({disabled, checked, onChange}) => {
    return (
        <li>
            <label>
                <input type="checkbox" name="" disabled={disabled} checked={checked} onChange={onChange} />
                <div className="icon">
                    <i className="fa fa-heart" aria-hidden="true"> </i>
                </div>
            </label>
        </li>
    )
}

export default BtnFollow;
