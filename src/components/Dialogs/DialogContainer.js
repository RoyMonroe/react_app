import {addMsgCreator} from "../../redux/messageReducer";
import Dialogs from "./Dialogs";
import {connect} from "react-redux";
import {withAuthRedirect} from "../../hoc/withAuthRedirect";
import {compose} from "redux";
//---------------------------------------------
let mapStateToProps = (state) => {
  return {
    dialogsData: state.usersData.dialogsData,
    messagesData: state.usersData.messagesData,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    addMsg: (message) => {
      dispatch(addMsgCreator(message));
    },
  };
};


export default compose(connect(mapStateToProps, mapDispatchToProps), withAuthRedirect)(Dialogs)
