import React from "react";
import {NavLink} from "react-router-dom";
import {useForm} from "react-hook-form";
//------------------------------------------
const Dialogs = (props) => {
  //FIXME: Не обновляется форма
  const {register, handleSubmit} = useForm();
  const onSubmit = data => {
    props.addMsg(data.messageArea);
  };

  const messagesElements = props.messagesData.map((msg, index) => {
    let message = msg.message;
    return (
      <div className="message" key={index}>
        {message}
      </div>
    );
  });

  const dialogsElement = props.dialogsData.map((dialog, index) => {
    let dialogName = dialog.name;
    let path = "/Messages/" + dialog.id;
    return (
      <div className="dialog" key={index}>
        <div className="dialog__inner">
          <img src={dialog.avatar} alt=""/>
          <div>
            <NavLink to={path} className="dialog__inner--name">
              {dialogName}
            </NavLink>
          </div>
        </div>
        <div className="messages open" id={dialog.id}>
          {messagesElements}
          <div className="textInput">
            <form className="messageForm" onSubmit={handleSubmit(onSubmit)}>
              <div className="messageForm__Area">
                <input type="string" name="messageArea" ref={register}/>
              </div>
              <div className="messageForm__btn">
                <button type="submit" className='btn btn__send'>Send</button>
              </div>
            </form>
          </div>

        </div>
      </div>
    );
  });

  return (
    <>
      <div>
        <input className="searchPanel" type="text"/>
      </div>
      <div className="dialog_list">{dialogsElement}</div>
    </>
  );
};

export default Dialogs;
