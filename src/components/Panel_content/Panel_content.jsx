import React from "react";
import Button from "../Button/Button";
import MessageIcon from "./MessageIcon";

const PanelContent = () => {
  let pushBtn = () => {
    alert("Hello, Roy");
  };
  return (
    <div className='content__inner--panel'>
      <Button onClick={pushBtn} className='btn--main'>
        <MessageIcon />
        <div className='btn--main--title'>First</div>
      </Button>
      <Button onClick={pushBtn} className='btn--main'>
        <MessageIcon />
        <div className='btn--main--title'>Second</div>
      </Button>
      <Button onClick={pushBtn} className='btn--main'>
        <MessageIcon />
        <div className='btn--main--title'>Third</div>
      </Button>
      <Button onClick={pushBtn} className='btn--main'>
        <MessageIcon />
        <div className='btn--main--title'>Fourth</div>
      </Button>
      <Button onClick={pushBtn} className='btn--main'>
        <MessageIcon />
        <div className='btn--main--title'>Fifth</div>
      </Button>
    </div>
  );
};

export default PanelContent;
