import React from "react";
import cover from "../../style/cover.jpg";
import Profile from "./Profile";
import {connect} from "react-redux";
import {getUserProfile, getUserStatus, updateStatus,savePhoto} from "../../redux/profileReducer"
import Preloader from "../Preloader/Preloader";
import {withRouter} from "react-router-dom";
import {compose} from "redux";
import {withAuthRedirect} from "../../hoc/withAuthRedirect";


//--------------------------------------------
class ProfileContainer extends React.Component {
  const
  refreshProfile = () => {
    let myUserId = this.props.myUserId;
    let userId = this.props.match.params.userId;
    this.props.getUserProfile(!userId ? myUserId : userId);
    this.props.getUserStatus(!userId ? myUserId : userId);
  }

  componentDidMount() {
    this.refreshProfile()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.match.params.userId !== prevProps.match.params.userId){
      this.refreshProfile()
    }
  }

  render() {
    if (!this.props.profile) {
      return <Preloader/>
    }
    return <Profile {...this.props} isOwner={!this.props.match.params.userId} cover={cover}/>

  }
}

let mapStateToProps = (state) => {
  return {
    profile: state.profilePage.profile,
    status: state.profilePage.status,
    myUserId: state.auth.userId
  };
};


export default compose(connect(mapStateToProps, {
  getUserProfile,
  getUserStatus,
  updateStatus,
  savePhoto,
}), withAuthRedirect, withRouter)
(ProfileContainer)
