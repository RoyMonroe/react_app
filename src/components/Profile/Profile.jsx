import React from "react";
import PanelContent from "../Panel_content/Panel_content";
import ProfileStatus from "./ProfileStatus"
import Preloader from "../Preloader/Preloader";
//----------------------------------------------
const Profile = React.memo((props) => {
  if (!props.profile) {
    return <Preloader/>
  }

  const onMainPhotoSelect = (e) => {
    if (e.target.files.length){
      props.savePhoto(e.target.files[0])
    }
  }
  return (
    <>
      <div className="cover">
        <div className="cover__photo">
          <img src={props.profile.photos.large || props.cover} alt=""/>
          <div>{props.isOwner && <input type={'file'} onChange={onMainPhotoSelect}/>}</div>
        </div>
        <div className="cover__info">
          <h3>{props.profile.fullName}</h3>
          <h4><ProfileStatus updateStatus={props.updateStatus} status={props.status}/></h4>
          <h5>{props.profile.aboutMe}</h5>
        </div>
        <div className="cover__social">
          <li><a href={"https://" + props.profile.contacts.vk}>Vk</a></li>
          <li><a href={"https://" + props.profile.contacts.facebook}>Facebook</a></li>
          <li><a href={"https://" + props.profile.contacts.instagram}>Instagram</a></li>
          <li><a href={"https://" + props.profile.contacts.github}>Github</a></li>
          <li><a href={"https://" + props.profile.contacts.twitter}>Twitter</a></li>
        </div>
      </div>
      <PanelContent/>
    </>
  );
});

export default Profile;
