import React from "react";

const Button = ({
    children, type, onClick, className, disabled
}) => {
  return (
    <button className={className} onClick={onClick} type={type}>
      {children}
    </button>
  );
};

export default Button;