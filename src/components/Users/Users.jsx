import React from "react";
import {NavLink} from "react-router-dom";
import BtnFollow from "../BtnFollow/BtnFollow";

//-----------------------------------------
const Users = (props) => {

  return (
    <div>
      <div className="users_list">
        {props.users.map(user => (
          <div key={user.id} className="user">
            <div className="user__avatar">
              <NavLink to={'/Profile/' + user.id}>
                <img src={user.photos.small || props.cover} alt=""/>
              </NavLink>
              <div>
                {user.followed ? <BtnFollow
                    disabled={props.followingInProgess.some(id => id === user.id)}
                    checked={user.followed}
                    onChange={() => {
                      props.unfollow(user.id)
                    }}/> :
                  <BtnFollow
                    disabled={props.followingInProgess.some(id => id === user.id)}
                    checked={user.followed}
                    onChange={() => {
                      props.follow(user.id)
                    }}
                  />}
              </div>
            </div>
            <div className="user__inner">
              <div className="user__info">
                <div className="user__info-name">{user.name}</div>
                <div
                  className="user__info-status">{user.status != null ? user.status : "There is my status"}</div>
              </div>
              <div className="user__location">
                <div className="user__location-country">
                  USA{/*{user.location.country}*/}
                </div>
                <div className="user__location-city">Denver{/*{user.location.city}*/}</div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default Users;
