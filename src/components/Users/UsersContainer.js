import {connect} from "react-redux";
import {
  follow,
  unfollow,
  setCurrentPage,
  setTotalUsersCount,
  requestUsers,
} from "../../redux/usersReducer";
import React from "react";
import Users from "./Users";
import Preloader from "../Preloader/Preloader";
import cover from "../../style/cover.jpg";
import {compose} from "redux";
import {
  getCurrentPage,
  getFollowingInProgess,
  getIsFetching,
  getPageSize,
  getTotalUsersCount,
  getUsers
} from "../../redux/selectors";

//----------------------------------------------
class UsersContainer extends React.Component {
  componentDidMount() {
    this.props.getUsers(this.props.currentPage, this.props.pageSize);
  }

  onPageCahged = (page) => {
    this.props.setCurrentPage(page);
    this.props.getUsers(page, this.props.pageSize);

  }

  render() {
    return <>
      {this.props.isFetching ? <Preloader/> :
        <Users
          totalUsersCount={this.props.totalUsersCount}
          pageSize={this.props.pageSize}
          currentPage={this.props.currentPage}
          onPageCahged={this.onPageCahged}
          users={this.props.users}
          follow={this.props.follow}
          unfollow={this.props.unfollow}
          cover={cover}
          followingInProgess={this.props.followingInProgess}
        />}
    </>
  }

}

let mapStateToProps = (state) => {
  return {
    users: getUsers(state),
    pageSize: getPageSize(state),
    totalUsersCount: getTotalUsersCount(state),
    currentPage: getCurrentPage(state),
    isFetching: getIsFetching(state),
    followingInProgess: getFollowingInProgess(state),
  };
};

export default compose(
  connect(mapStateToProps, {follow, unfollow, setCurrentPage, setTotalUsersCount, getUsers: requestUsers,}))
(UsersContainer)
