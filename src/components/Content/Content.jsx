import React from "react";
import {NavLink, Route, Switch} from "react-router-dom";
import Preloader from "../Preloader/Preloader";
import ReactPaginationNav from 'react-pagination-nav'

const LoginNew = React.lazy(() => import('../Login/Login'));
const ProfileContainer = React.lazy(() => import('../Profile/ProfileContainer'));
const UsersContainer = React.lazy(() => import('../Users/UsersContainer.js'));
const DialogContainer = React.lazy(() => import('../Dialogs/DialogContainer.js'));
//---------------------------------------------

const Content = (props) => {
  let pagesCount = Math.ceil(props.totalUsersCount / props.pageSize);

  return (
    <div className="content">
      <div className="content__title">
        <h3>Main Panel</h3>
        {props.isAuth ? <button onClick={props.logout} className="btn--settings">Logout</button> :
          <button className="btn--settings"><NavLink to="/Login">Login</NavLink></button>}
      </div>
      <div className="content__inner">
        <React.Suspense fallback={<Preloader/>}>
          <Switch>
            <Route path="/Profile/:userId?" render={() => <ProfileContainer/>}/>
            <Route path="/Messages" render={() => <DialogContainer/>}/>
            <Route path='/Search' render={() => <>
              <ReactPaginationNav
                className="paginator"
                pageCount={pagesCount}
                visiblePages={8}
                currentPage={props.currentPage}
                goToNextPage={(newPage) => props.onPageCahged(pagesCount)}
                goToPreviousPage={() => props.onPageCahged(1)}
                goToPage={(newPage) => props.onPageCahged(newPage)}
              />
              <UsersContainer/>
            </>}/>
            <Route path='/Login' render={() => <LoginNew/>}/>
          </Switch>
        </React.Suspense>
      </div>
    </div>
  );
};

export default Content;
