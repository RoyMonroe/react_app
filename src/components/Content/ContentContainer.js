import React from "react";
import Content from "./Content";
import {connect} from "react-redux";
import {login, logout} from "../../redux/authReducer";
import {compose} from "redux";
import {getCurrentPage, getPageSize, getTotalUsersCount} from "../../redux/selectors";
import {requestUsers, setCurrentPage, setTotalUsersCount} from "../../redux/usersReducer";

//---------------------------------------------

class ContentContainer extends React.Component {
  onPageCahged = (page) => {
    this.props.setCurrentPage(page);
    this.props.getUsers(page, this.props.pageSize);

  }

  render() {

    return <Content {...this.props} onPageCahged={this.onPageCahged}/>
  }
}

const mapStateToProps = (state) => ({
  isAuth: state.auth.isAuth,
  myLogin: state.auth.login,
  pageSize: getPageSize(state),
  totalUsersCount: getTotalUsersCount(state),
  currentPage: getCurrentPage(state),
})
export default compose(connect(mapStateToProps, {
  login,
  logout,
  setCurrentPage,
  setTotalUsersCount,
  getUsers: requestUsers,
}))(ContentContainer)
