import React from "react";
import {useForm} from "react-hook-form";
import {connect} from "react-redux";
import {getCaptcha, login, logout} from "../../redux/authReducer";
import {Redirect} from "react-router-dom";
import "./_Login.scss";
//-----------------------------------------------------

const Login = React.memo(({login, isAuth, messageError, captchaUrl}) => {
    const {register, handleSubmit, errors} = useForm();
    const onSubmit = (props) => {

      login(props.email, props.password, props.rememberMe, props.captcha)

    };
    if (isAuth) {
      return <Redirect to={"/Profile"}/>
    }

    const panelOn = () => {
      document.getElementById('containerLogin').classList.add("right-panel-active")
    }
    const panelOff = () => {
      document.getElementById('containerLogin').classList.remove("right-panel-active")
    }
    return (

      <>
        <div className="containerLogin " id="containerLogin">
          <div className="form-container sign-up-container">
            <form onSubmit={handleSubmit(onSubmit)}>
              <h1>Create Account</h1>
              <div className="social-container">
                {/*<a href="#" className="social"><i className="fab fa-facebook-f"/></a>*/}
                {/*<a href="#" className="social"><i className="fab fa-google-plus-g"/></a>*/}
                {/*<a href="#" className="social"><i className="fab fa-linkedin-in"/></a>*/}
              </div>
              <span>or use your email for registration</span>
              <input type="text" placeholder="Name" name="name"/>
              <input type="email" name="email" placeholder="Email" ref={register({required: "This is required"})}/>
              <input type="password" name="password" placeholder="Password"
                     ref={register({required: true, minLength: 6})}/>
              <button>Sign Up</button>
            </form>
          </div>
          <div className="form-container sign-in-container">
            <form onSubmit={handleSubmit(onSubmit)}>
              <h1>Sign in</h1>
              <div className="social-container">
                {/*<a href="#" className="social"><i className="fab fa-facebook-f"/></a>*/}
                {/*<a href="#" className="social"><i className="fab fa-google-plus-g"/></a>*/}
                {/*<a href="#" className="social"><i className="fab fa-linkedin-in"/></a>*/}
              </div>
              <span>or use your account</span>
              <div className="input">
                <label htmlFor="email"/>
                <input
                  name="email"
                  placeholder="Email"
                  type="email"
                  ref={register({required: "This is required"})}/>
                {errors.email?.message}
              </div>
              <div className="input">
                <label htmlFor="password"/>
                <input type="password" name="password" ref={register({required: true, minLength: 6})}/>
                {errors.password?.type === "required" && "Your input is required"}
                {errors.password?.type === "minLength" && "Minimal length 6 symbols"}
              </div>
              <div className="rememberMe">
                <label htmlFor="rememberMe">Remember me</label>
                <input
                  type="checkbox"
                  placeholder="Password"
                  name="rememberMe"
                  ref={register}
                />
              </div>
              <div className="messageError">{messageError}<img src={captchaUrl} alt=""/></div>
              {captchaUrl && <input type="captcha" name="captcha" ref={register}/>}
              <button type="submit" name="Login">Sign In</button>
            </form>
          </div>
          <div className="overlay-container">
            <div className="overlay">
              <div className="overlay-panel overlay-left">
                <h1>Welcome Back!</h1>
                <p>To keep connected with us please login with your personal info</p>
                <button onClick={panelOff} className="ghost" id="signIn">Sign In</button>
              </div>
              <div className="overlay-panel overlay-right">
                <h1>Hello, Friend!</h1>
                <p>Enter your personal details and start journey with us</p>
                <button onClick={panelOn} className="ghost" id="signUp">Sign Up</button>
              </div>
            </div>
          </div>
        </div>
      </>
    )
  }
)
const mapStateToProps = (state) => ({
  isAuth: state.auth.isAuth,
  messageError: state.auth.messageError,
  captchaUrl:state.auth.captchaUrl,
})
export default connect(mapStateToProps, {login, logout,captcha: getCaptcha})(Login);


