import { authAPI } from "../api/api";
//-------------------------------------
const SET_USER_DATA = "myapp/auth/SET_USER_DATA";
const SET_ERROR_MESSAGE = "myapp/auth/SET_ERROR_MESSAGE";
const SET_CAPTCHA = "myapp/auth/SET_CAPTCHA";
//--------------------------------------
export type IntitialStateType = typeof initialState;
type PayloadType = {
    userId: number | null;
    email: string | null;
    login: string | null;
    isAuth: boolean | null;
};
type SetUserdataType = {
    type: typeof SET_USER_DATA;
    payload: PayloadType;
};
type SetCapchaUrlType = {
    type: typeof SET_CAPTCHA;
    payload: { captchaUrl: string };
};
export const setUserData = (
    userId: number | null,
    email: string | null,
    login: string | null,
    isAuth: boolean
): SetUserdataType => ({
    type: SET_USER_DATA,
    payload: { userId, email, login, isAuth }
});
export const setMessageError = (message: string) => ({
    type: SET_ERROR_MESSAGE,
    message
});
export const setCaptchaUrl = (captchaUrl: string): SetCapchaUrlType => ({
    type: SET_CAPTCHA,
    payload: { captchaUrl }
});
//------------------------------------------------
let initialState = {
    userId: null as number | null,
    email: null as string | null,
    login: null as string | null,
    messageError: null as string | null,
    captchaUrl: null as string | null
};

const authReducer = (state = initialState, action: any): IntitialStateType => {
    switch (action.type) {
        case SET_USER_DATA:
            return {
                userID: "weqw",
                ...state,
                ...action.payload
            };
        case SET_ERROR_MESSAGE:
            return {
                ...state,
                messageError: action.message
            };
        case SET_CAPTCHA:
            return {
                ...state,
                ...action.payload
            };
        default:
            return state;
    }
};
//------------------Thunks-------------------
export const getProfileAuthInfo = () => async (dispatch: any) => {
    let response = await authAPI.getProfileInfo();
    if (response.resultCode === 0) {
        let { id, email, login } = response.data;
        dispatch(setUserData(id, email, login, true));
    }
};

export const login = (
    email: string,
    password: string,
    rememberMe: boolean,
    captcha: string
) => async (dispatch: any) => {
    let response = await authAPI.login(email, password, rememberMe, captcha);
    if (response.data.resultCode === 0) {
        dispatch(getProfileAuthInfo());
    }
    if (response.data.resultCode === 1) {
        let messageError = response.data.messages;
        dispatch(setMessageError(messageError));
    } else {
        if (response.data.resultCode === 10) {
            dispatch(getCaptcha());
        }
    }
};

export const logout = () => async (dispatch: any) => {
    let response = await authAPI.logout();
    if (response.data.resultCode === 0) {
        dispatch(setUserData(null, null, null, false));
    }
};

export const getCaptcha = () => async (dispatch: any) => {
    const response = await authAPI.getCapchaUrl();
    dispatch(setCaptchaUrl(response.data.url));
};
//-------------------------------------------
export default authReducer;
