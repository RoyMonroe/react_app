import { usersAPI } from "../api/api";
import { updateObjectInArray } from "../utils/object-helpers";
import { UsersType } from "../types/types";

// -------------------------------------------
const FOLLOW = "FOLLOW";
const UNFOLLOW = "UNFOLLOW";
const SET_USERS = "SET_USERS";
const SET_CURRENT_PAGE = "SET_CURRENT_PAGE";
const SET_TOTAL_USERS_COUNT = "SET_TOTAL_USERS_COUNT";
const TOGGLE_IS_FETCHING = "TOGGLE_IS_FETCHING";
const TOGGLE_IS_FOLLOWING_PROGRESS = "TOGGLE_IS_FOLLOWING_PROGRESS";
// -------------------------------------------
type FollowSuccessType = {
    type: typeof FOLLOW;
    userID: number;
};
type UnfollowSuccessType = {
    type: typeof UNFOLLOW;
    userID: number;
};
type SetUsersType = {
    type: typeof SET_USERS;
    users: UsersType;
};
type SetCurrentPageType = {
    type: typeof SET_CURRENT_PAGE;
    currentPage: number;
};
type SetTotalUsersCountType = {
    type: typeof SET_TOTAL_USERS_COUNT;
    totalCount: number;
};
type ToggleIsFetchingType = {
    type: typeof TOGGLE_IS_FETCHING;
    isFetching: boolean;
};
type ToggleFollowingProgressType = {
    type: typeof TOGGLE_IS_FOLLOWING_PROGRESS;
    isFetching: boolean;
    userId: number;
};
// ----------------ACTIONS--------------------
export const followSuccess = (userID: number): FollowSuccessType => ({ type: FOLLOW, userID });
export const unfollowSuccess = (userID: number): UnfollowSuccessType => ({
    type: UNFOLLOW,
    userID
});
// @ts-ignore
export const setUsers = (users:Array<UsersType>): SetUsersType => ({ type: SET_USERS, users });
export const setCurrentPage = (currentPage: number): SetCurrentPageType => ({
    type: SET_CURRENT_PAGE,
    currentPage: currentPage
});
export const setTotalUsersCount = (totalUsersCount: number): SetTotalUsersCountType => ({
    type: SET_TOTAL_USERS_COUNT,
    totalCount: totalUsersCount
});
export const toggleIsFetching = (isFetching: boolean): ToggleIsFetchingType => ({
    type: TOGGLE_IS_FETCHING,
    isFetching
});
export const toggleFollowingProgress = (
    isFetching: boolean,
    userId: number
): ToggleFollowingProgressType => ({
    type: TOGGLE_IS_FOLLOWING_PROGRESS,
    isFetching,
    userId
});
type IntitialStateType = typeof initialState;
//--------------------------------------------
let initialState = {
    users: [] as Array<UsersType>,
    pageSize: 12,
    totalUsersCount: "",
    currentPage: 1,
    isFetching: false,
    followingInProgess: [] as Array<number> //Array of usersId
};

const usersReducer = (state = initialState, action: any):IntitialStateType => {
    switch (action.type) {
        case FOLLOW:
            return {
                ...state,
                users: updateObjectInArray(state.users, action.userID, "id", {
                    followed: true
                })
            };

        case UNFOLLOW:
            return {
                ...state,
                users: updateObjectInArray(state.users, action.userID, "id", {
                    followed: false
                })
            };
        case SET_USERS:
            return {
                ...state,
                users: action.users
            };
        case SET_CURRENT_PAGE:
            return {
                ...state,
                currentPage: action.currentPage
            };
        case SET_TOTAL_USERS_COUNT:
            return {
                ...state,
                totalUsersCount: action.totalCount
            };
        case TOGGLE_IS_FETCHING:
            return {
                ...state,
                isFetching: action.isFetching
            };
        case TOGGLE_IS_FOLLOWING_PROGRESS:
            return {
                ...state,
                followingInProgess: action.isFetching
                    ? [...state.followingInProgess, action.userId]
                    : state.followingInProgess.filter(id => id !== action.userId)
            };
        default:
            return state;
    }
};
//------------------Thunks-------------------

const followToggle = async (dispatch: any, userId: number, apiMethod: any, actionCreator: any) => {
    dispatch(toggleFollowingProgress(true, userId));
    let response = await apiMethod(userId);
    if (response.resultCode === 0) {
        dispatch(actionCreator(userId));
    }
    dispatch(toggleFollowingProgress(false, userId));
};

export const requestUsers = (currentPage: number, pageSize: number) => {
    return async (dispatch: any) => {
        dispatch(toggleIsFetching(true));
        let response = await usersAPI.getUsers(currentPage, pageSize);
        dispatch(toggleIsFetching(false));
        dispatch(setUsers(response.items));
        dispatch(setTotalUsersCount(response.totalCount));
    };
};
export const follow = (userId: number) => {
    return async (dispatch: any) => {
        await followToggle(dispatch, userId, usersAPI.getFollow.bind(usersAPI), followSuccess);
    };
};
export const unfollow = (userId: number) => {
    return async (dispatch: any) => {
        await followToggle(dispatch, userId, usersAPI.getUnfollow.bind(usersAPI), unfollowSuccess);
    };
};
//------------------------------------------

export default usersReducer;
