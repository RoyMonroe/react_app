import { profileAPI, usersAPI } from "../api/api";
import { ProfileType, PhotosType, ContactsType } from "../types/types";
//---------------------------------------
const SET_USER_PROFILE = "SET_USER_PROFILE";
const SET_USER_STATUS = "SET_USER_STATUS";
const SAVE_PHOTO_SUCCESS = "SAVE_PHOTO_SUCCESS";

//---------------------------------------
type SetUserProfileType = {
    type: typeof SET_USER_PROFILE;
    profile: ProfileType;
};
type SetUserStatusType = {
    type: typeof SET_USER_STATUS;
    status: string;
};
type SavePhotoSuccessType = {
    type: typeof SAVE_PHOTO_SUCCESS;
    photos: PhotosType;
};

export type IntitialStateType = typeof initialState;
//---------------------------------------
export const setUserProfile = (profile: ProfileType): SetUserProfileType => ({
    type: SET_USER_PROFILE,
    profile
});
export const setUserStatus = (status: string): SetUserStatusType => ({
    type: SET_USER_STATUS,
    status
});
export const savePhotoSuccess = (photos: PhotosType): SavePhotoSuccessType => ({
    type: SAVE_PHOTO_SUCCESS,
    photos
});
//---------------------------------------
let initialState = {
    profile: null as ProfileType | null,
    status: "...",
    isFetching: true
};

const profileReducer = (state = initialState, action: any): IntitialStateType => {
    switch (action.type) {
        case SET_USER_PROFILE: {
            return {
                ...state,
                profile: action.profile
            };
        }
        case SET_USER_STATUS: {
            return {
                ...state,
                status: action.status
            };
        }
        case SAVE_PHOTO_SUCCESS: {
            return {
                ...state,
                profile: { ...state.profile, photos: action.photos } as ProfileType
            };
        }
        default: {
            return state;
        }
    }
};
//------------------Thunks-------------------
export const getUserProfile = (userId: number) => async (dispatch: any) => {
    let response = await usersAPI.getUserProfile(userId);
    dispatch(setUserProfile(response));
};
export const getUserStatus = (userId: number) => async (dispatch: any) => {
    let response = await profileAPI.getStatus(userId);
    dispatch(setUserStatus(response.data));
};
export const updateStatus = (status: string) => async (dispatch: any) => {
    let response = await profileAPI.updateStatus(status);
    if (response.data.resultCode === 0) {
        dispatch(setUserStatus(status));
    }
};

export const savePhoto = (file: string) => async (dispatch: any) => {
    let response = await profileAPI.savePhoto(file);
    if (response.data.resultCode === 0) {
        dispatch(savePhotoSuccess(response.data.data.photos));
    }
};
//-----------------------------------------
export default profileReducer;
