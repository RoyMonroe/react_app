import cover from "../style/cover.jpg";
//-------------------------------------
const ADD_MSG = "ADD_MSG";
//-------------------------------------
export type IntitialStateType = typeof initialState;
type DialogType = {
    id: number;
    name: string;
};
type MessageType = {
    id: number;
    message: string;
};
type AddMsgCreatorType = {
    type: typeof ADD_MSG;
    message: string;
};
//-------------------------------------
export const addMsgCreator = (message: string): AddMsgCreatorType => {
    return { type: ADD_MSG, message: message };
};

let initialState = {
    dialogsData: [
        { id: 1, name: "Rachell Monroe" }
        // {id: 2, name: "BookerT"},
        // {id: 3, name: "Husein"},
    ] as Array<DialogType>,
    messagesData: [
        { id: 1, message: "Проверка сообщения" },
        { id: 2, message: "Проверка сообщения2" }
    ] as Array<MessageType>,
    friendsData: [
        { id: 1, name: "Rachell Monroe" },
        { id: 2, name: "Richard" },
        { id: 3, name: "Caroline" },
        { id: 4, name: "Francis" },
        { id: 5, name: "Lucas" },
        { id: 6, name: "Manny" }
    ]
};

const messageReducer = (state = initialState, action: any): IntitialStateType => {
    switch (action.type) {
        case ADD_MSG:
            return {
                ...state,
                messagesData: [...state.messagesData, { id: 6, message: action.message }]
            };
        default:
            return state;
    }
};

export default messageReducer;
