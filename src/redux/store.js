import messageReducer from "./messageReducer";

let store = {
  _state: {
    usersData: {
      dialogsData: [
        { id: 1, name: "Rachell Monroe" },
        { id: 2, name: "Richard" },
        { id: 3, name: "Caroline" },
      ],
      messagesData: {
        newMessage: "",
        messagesHistory: [
          { id: 1, message: "Проверка сообщения" },
          { id: 2, message: "Проверка сообщения 2" },
          { id: 3, message: "Проверка сообщения 3" },
        ],
      },
      friendsData: [
        { id: 1, name: "Rachell Monroe" },
        { id: 2, name: "Richard" },
        { id: 3, name: "Caroline" },
        { id: 4, name: "Francis" },
        { id: 5, name: "Lucas" },
        { id: 6, name: "Manny" },
      ],
    },
  },
  getState() {
    return this._state;
  },
  _rerenderApp() {},
  subscribe(observer) {
    this._rerenderApp = observer;
  },
  dispatch(action) {
    this._state.usersData.messagesData = messageReducer(
      this._state.usersData.messagesData,
      action
    );
    this._rerenderApp(this._state);
  },
};
export default store;
window.store = store;
