import { getProfileAuthInfo } from "./authReducer";
//----------------------------------------------
const INITIALIZED_SUCCESS = "INITIALIZED_SUCCESS";
// ----------------ACTIONS--------------------
type InitializedSuccessType = {
    type: typeof INITIALIZED_SUCCESS;
};
export const setInitializedSuccess = (): InitializedSuccessType => ({
    type: INITIALIZED_SUCCESS
});
//----------------------------------------------
type IntitialStateType = {
    initialized: boolean;
};
let initialState: IntitialStateType = {
    initialized: false
};

const appReducer = (state = initialState, action: any): IntitialStateType => {
    switch (action.type) {
        case INITIALIZED_SUCCESS:
            return {
                ...state,
                initialized: true
            };
        default:
            return state;
    }
};
//------------------Thunks-------------------
export const initializeApp = () => async (dispatch: any) => {
    await dispatch(getProfileAuthInfo());
    dispatch(setInitializedSuccess());
};
//------------------------------------------
export default appReducer;
