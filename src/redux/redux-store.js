import {createStore, combineReducers, applyMiddleware} from "redux";
import messageReducer from "./messageReducer";
import usersReducer from "./usersReducer";
import profileReducer from "./profileReducer";
import authReducer from "./authReducer";
import appReducer from "./appReducer";
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from 'redux-devtools-extension';
//-----------------------------------------------
let redusers = combineReducers({
    usersData: messageReducer,
    usersPage: usersReducer,
    profilePage: profileReducer,
    auth: authReducer,
    app: appReducer,
});
const store = createStore(redusers, composeWithDevTools(applyMiddleware(thunkMiddleware),));
window.store = store;
export default store;
