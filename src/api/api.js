import * as axios from "axios";
//---------------------------------------------
const instance = axios.create({
    withCredentials: true,
    baseURL: "https://social-network.samuraijs.com/api/1.0/",
    headers: { "API-KEY": "e8c0b72c-eeb7-4536-982d-a185ad2a7735" }
});
//----------------------usersAPI------------------------

export const usersAPI = {
    getUsers(currentPage = 1, pageSize = 12) {
        return instance
            .get(`users?page=${currentPage}&count=${pageSize}`)
            .then(response => {
                return response.data;
            });
    },
    getUserProfile(userId) {
        return instance.get(`profile/` + userId).then(response => {
            return response.data;
        });
    },
    getFollow(userId) {
        return instance.post(`follow/${userId}`).then(response => {
            return response.data;
        });
    },
    getUnfollow(userId) {
        return instance.delete(`follow/${userId}`).then(response => {
            return response.data;
        });
    }
};
//----------------------profileAPI------------------------
export const profileAPI = {
    getStatus(userId) {
        return instance.get("profile/status/" + userId);
    },
    updateStatus(status) {
        return instance.put("profile/status", { status: status });
    },
    savePhoto(photoFile) {
        const formData = new FormData();
        formData.append("image", photoFile);
        return instance.put("profile/photo", formData, {
            headers: {
                "Content-Type": "multipart/form-data"
            }
        });
    }
};
//----------------------authAPI------------------------
export const authAPI = {
    getProfileInfo() {
        return instance.get(`auth/me`).then(response => {
            return response.data;
        });
    },
    login(email, password, rememberMe = false, captcha) {
        return instance.post("auth/login", {
            email,
            password,
            rememberMe,
            captcha
        });
    },
    logout() {
        return instance.delete("auth/login");
    },

    getCapchaUrl() {
        return instance.get(`security/get-captcha-url`);
    }
};
