import React from "react";
import {compose} from "redux";
import {connect} from "react-redux";
import SideBar from "./components/Side-bar/Side-bar";
import ContentContainer from "./components/Content/ContentContainer";
import {initializeApp} from "./redux/appReducer";
import Preloader from "./components/Preloader/Preloader";
import {withRouter} from "react-router-dom";



class App extends React.Component {
  componentDidMount() {
    this.props.initializeApp()

  }

  render() {
    if (!this.props.initialized) {
      return <Preloader/>
    }
    return (
        <div className="app-wrapper">
          <SideBar/>
          <ContentContainer/>
        </div>

    );
  }
}

const mapStateToProps = (state) => ({
  initialized: state.app.initialized
})
export default compose(
 withRouter,
  connect(mapStateToProps, {initializeApp}))(App)
